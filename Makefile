
build:
	go build -o ck-simulation

test: test-unit test-integration

test-unit:
	go test ./pkg/... -cover

test-integration:
	go test ./integration/e2e_test.go -cover

run: build
	./ck-simulation --config docs/examples/simple/config.yml --order-file docs/examples/simple/orders.json

lint:
	golangci-lint run -E goimports --fix

clean:
	@rm -f ck-simulation
