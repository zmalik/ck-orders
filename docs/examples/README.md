# Examples

There are three examples to show few features in the simulation

## Simple
```
./ck-simulation --order-file docs/examples/simple/orders.json --config docs/examples/simple/config.yml
```

## Slow Couriers
```
./ck-simulation --order-file docs/examples/slow-deliveries/orders.json --config docs/examples/slow-deliveries/config.yml
```

### Short Lived Orders
```
./ck-simulation --order-file docs/examples/short-lived-orders/orders.json --config docs/examples/short-lived-orders/config.yml
```
