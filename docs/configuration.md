# Configuring the Simulation system

## Flags

The next flags can be passed to simulation binary

- `--config` the configuration file, default is `docs/examples/simple/config.yml`
- `--orders-file` the orders file, default is empty
- `--orders-channel-buffer` the orders channel buffer size, default to `10` 

## Configuration file 

The simulation can be configured using the config file that is passed using the flag `--config`

The configuration can define 
- the shelves available in the Kitchen
- the orders read rate
- the delivery couriers arrival frequency 

Example:
```
kitchen:
  orders:
    rate: 2
    timeUnit: 1s
  shelves:
    - name: Hot shelf
      temperature: hot
      capacity: 10
      decayModifier: 1
    - name: Cold shelf
      temperature: cold
      capacity: 10
      decayModifier: 1
    - name: Frozen shelf
      temperature: frozen
      capacity: 10
      decayModifier: 1
    - name: Overflow shelf
      temperature: any
      capacity: 15
      decayModifier: 2
delivery:
  courier:
    rateMin: 2
    rateMax: 6
    timeUnit: 1s
```

