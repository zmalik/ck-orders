# Simulations


The projects comes with three different simulations

- simple (most orders are picked up)
- slow-couriers (couriers come late and many orders expires waiting)
- short-lived-orders (orders shelf-life is really short)


## Build the project

```
make build
```

## Simple

```
./ck-simulation --order-file docs/examples/simple/orders.json --config docs/examples/simple/config.yml
```

## Slow Couriers

Couriers start really slow, making some orders overflow in the shelves.

```
./ck-simulation --order-file docs/examples/slow-deliveries/orders.json --config docs/examples/slow-deliveries/config.yml
```

### Short Lived Orders

Orders shelf life is really low, making orders expire fast.
```
./ck-simulation --order-file docs/examples/short-lived-orders/orders.json --config docs/examples/short-lived-orders/config.yml
```
