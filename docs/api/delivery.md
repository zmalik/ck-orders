# delivery
--
    import "github.com/zmalik/ck-orders/pkg/delivery"


## Usage

#### type Config

```go
type Config struct {
	Delivery Delivery `yaml:"delivery"`
}
```


#### type CourierConfig

```go
type CourierConfig struct {
	RateMin  int           `yaml:"rateMin"`
	RateMax  int           `yaml:"rateMax"`
	TimeUnit time.Duration `yaml:"timeUnit"`
}
```


#### type Delivery

```go
type Delivery struct {
	CourierConfig CourierConfig `yaml:"courier"`
}
```


#### type Service

```go
type Service struct {
	ConfigFilePath *string // path to the Delivery Config file
	KitchenService *kitchen.Service

	OrdersChan chan *order.Order

	MarkPickedOrders bool
}
```

Delivery Service watches the orders channel for pick up And sends courier with
random delay inside configured range

#### func (*Service) Start

```go
func (s *Service) Start() error
```
Starts the Delivery Service
