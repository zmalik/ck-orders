# kitchen
--
    import "github.com/zmalik/ck-orders/pkg/kitchen"


## Usage

#### type Config

```go
type Config struct {
	Kitchen Kitchen `yaml:"kitchen"`
}
```


#### type Kitchen

```go
type Kitchen struct {
	OrdersConfig OrdersConfig  `yaml:"orders"`
	ShelvesList  []shelf.Shelf `yaml:"shelves"`
}
```


#### type OrdersConfig

```go
type OrdersConfig struct {
	Rate     int           `yaml:"rate"`
	TimeUnit time.Duration `yaml:"timeUnit"`
}
```


#### type Service

```go
type Service struct {
	ConfigFilePath *string // path to the Kitchen Config file

	OrderReader  *order.FileOrderReader
	ShelfService *shelf.Service
	OrdersChan   chan *order.Order
	Stop         bool
}
```


#### func (*Service) PickUpOrder

```go
func (s *Service) PickUpOrder(order *order.Order) error
```
PickUpOrder picks up an order and removes it from the shelves

#### func (*Service) Start

```go
func (s *Service) Start() error
```
Starts the kitchen service
