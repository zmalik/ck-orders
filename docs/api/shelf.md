# shelf
--
    import "github.com/zmalik/ck-orders/pkg/shelf"


## Usage

```go
const (
	OverFlowShelfTemperature = "any"
)
```

#### type Service

```go
type Service struct {
	Shelves map[string]*Shelf // Shelves organised by temperature
}
```


#### func (*Service) AddToShelf

```go
func (s *Service) AddToShelf(order *order.Order)
```
AddToShelf adds an order to the designated or overflow shelf

#### func (*Service) Start

```go
func (s *Service) Start(shelvesList []Shelf)
```

#### func (*Service) ThrowExpiredOrders

```go
func (s *Service) ThrowExpiredOrders()
```
ThrowExpiredOrders check for all orders in all the shelves and removes the
expired orders

#### type Shelf

```go
type Shelf struct {
	Name          string `yaml:"name"`
	Temperature   string `yaml:"temperature"`
	Capacity      int    `yaml:"capacity"`
	DecayModifier int    `yaml:"decayModifier"`
}
```


#### func (*Shelf) HasCapacity

```go
func (s *Shelf) HasCapacity() bool
```
HasCapacity checks if the shelf has capacity for orders

#### func (*Shelf) RetrieveOrder

```go
func (s *Shelf) RetrieveOrder(orderID string) bool
```
RetrieveOrder removes the order from the shelf
