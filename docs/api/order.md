# order
--
    import "github.com/zmalik/ck-orders/pkg/order"


## Usage

#### type FileOrderReader

```go
type FileOrderReader struct {
	OrdersPath *string // path to the orders file

	Pause bool // pause the orders, use for graceful shutdown of the service
}
```


#### func (*FileOrderReader) LoadOrders

```go
func (f *FileOrderReader) LoadOrders() error
```
LoadOrders loads the orders present in f.OrdersPath file

#### func (*FileOrderReader) ReadNextOrders

```go
func (f *FileOrderReader) ReadNextOrders(count int) ([]*Order, error)
```
ReadNextOrders returns the next orders if there are not enough next orders, it
returns the remaining orders if there are no orders, it returns an empty list

#### type Order

```go
type Order struct {
	ID               string  `json:"id"`        // identifier of the order
	Name             string  `json:"name"`      // name of the dish
	Temperature      string  `json:"temp"`      // Preferred shelf storage temperature
	ShelfLifeSeconds int     `json:"shelfLife"` // Shelf wait max duration (seconds)
	DecayRate        float64 `json:"decayRate"` // Value deterioration modifier
}
```

Order represents a delivery order

#### func (*Order) GetLifeSeconds

```go
func (o *Order) GetLifeSeconds() int
```
GetLifeSeconds returns the current life of ther order in seconds

#### func (*Order) GetOrderValue

```go
func (o *Order) GetOrderValue() float64
```
GetOrderValue returns the current inherent value of the order

#### func (*Order) IncrementAge

```go
func (o *Order) IncrementAge()
```
IncrementAge increases the age of the order by one second

#### func (*Order) SetShelfDecayModifier

```go
func (o *Order) SetShelfDecayModifier(shelfDecayModifier int)
```
SetShelfDecayModifier sets the shelfDecayModifier for the order
