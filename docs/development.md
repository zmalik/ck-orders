# Development Guidelines

This document aims to place guidelines that build a simple and transparent mechanism for determining how to develop the Kitchen Simulation Project. 


### Linter

The project uses `golangci-lint` Go linters aggregator. Please [install the version](https://golangci-lint.run/usage/install/#local-installation) `1.30.0` before adding code to the project

```
make lint
```

### Integration testing

For the integration testing the project uses golden files. 
The integration tests should be added to `integration` directory. 

```
make test-integration
```

### Unit testing

Each package and service comes with its own unit tests.
When adding a new feature or new service, please make sure to add the
corresponding tests.

```
make test-unit
```

