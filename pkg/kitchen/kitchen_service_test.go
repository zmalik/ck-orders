package kitchen

import (
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/zmalik/ck-orders/pkg/order"
)

func TestService_GetConfig(t *testing.T) {
	testOrderPath := "testdata/testconfig.yaml"
	ks := Service{
		ConfigFilePath: &testOrderPath,
		OrderReader:    &order.FileOrderReader{},
	}

	err := ks.Start()
	assert.Nil(t, err, "Kitchen should start without error")

	config := ks.getConfig()

	assert.Equal(t, 2, config.Kitchen.OrdersConfig.Rate, "Order rate should be 2")
	assert.Equal(t, 1*time.Second, config.Kitchen.OrdersConfig.TimeUnit, "Time unit should be 1 sec")
	assert.Equal(t, 4, len(config.Kitchen.ShelvesList), "ShelvesList should have length 4")
	assert.Equal(t, "Hot shelf", config.Kitchen.ShelvesList[0].Name, "First shelf name should be Hot shelf")
	assert.Equal(t, "hot", config.Kitchen.ShelvesList[0].Temperature, "First shelf temperature should be hot")
}

func TestService_NonExistentConfig(t *testing.T) {
	testOrderPath := "testdata/non-existent.yaml"
	ks := Service{
		ConfigFilePath: &testOrderPath,
	}

	err := ks.Start()
	assert.NotNil(t, err, "Kitchen service should return error")
}

func TestService_MalformedConfig(t *testing.T) {
	testOrderPath := "testdata/malformedconfig.yaml"
	ks := Service{
		ConfigFilePath: &testOrderPath,
	}

	err := ks.Start()
	assert.NotNil(t, err, "Kitchen service should return error")
}

func TestService_RunKitchen(t *testing.T) {
	testConfigPath := "testdata/quickconfig.yaml"
	testOrdersPath := "testdata/orders.json"
	ordersIDs := make(chan *order.Order, 10)
	orderReader := &order.FileOrderReader{
		OrdersPath: &testOrdersPath,
	}
	if err := orderReader.LoadOrders(); err != nil {
		log.Fatalf("Error loading the orders: %v", err)
	}

	ks := Service{
		ConfigFilePath: &testConfigPath,
		OrderReader:    orderReader,
		OrdersChan:     ordersIDs,
	}

	err := ks.Start()
	assert.Nil(t, err, "Kitchen should start without error")

	time.Sleep(1 * time.Second)
	assert.False(t, ks.ShelfService.Shelves["frozen"].HasCapacity())

}
