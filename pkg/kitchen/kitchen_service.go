package kitchen

import (
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/zmalik/ck-orders/pkg/order"
	"github.com/zmalik/ck-orders/pkg/shelf"
	"gopkg.in/yaml.v3"
)

type Service struct {
	ConfigFilePath *string // path to the Kitchen Config file
	config         *Config
	OrderReader    *order.FileOrderReader
	ShelfService   *shelf.Service
	OrdersChan     chan *order.Order
	Stop           bool
}

// Starts the kitchen service
func (s *Service) Start() error {
	if s.config == nil {
		log.Infof("Initializing the Kitchen configuration from: %s", *s.ConfigFilePath)
		err := s.initialize()
		if err != nil {
			log.Errorf("Error loading Kitchen configuration from %s", *s.ConfigFilePath)
			return err
		}
		log.Infof("Kitchen Configuration loaded.")
	}

	go func() {
		for {
			<-time.After(s.config.Kitchen.OrdersConfig.TimeUnit)
			if s.Stop {
				break
			}
			go s.receiveOrders()
		}
	}()

	go func() {
		for {
			<-time.After(s.config.Kitchen.OrdersConfig.TimeUnit)
			if s.Stop {
				break
			}
			go s.ShelfService.ThrowExpiredOrders()
		}
	}()
	return nil
}

// PickUpOrder picks up an order and removes it from the shelves
func (s *Service) PickUpOrder(order *order.Order) error {
	if s.ShelfService.Shelves[order.Temperature].RetrieveOrder(order.ID) ||
		s.ShelfService.Shelves[shelf.OverFlowShelfTemperature].RetrieveOrder(order.ID) {
		return nil
	}
	return fmt.Errorf("order not found %s", order.ID)
}

func (s *Service) initialize() error {
	file, err := os.Open(*s.ConfigFilePath)
	if err != nil {
		return fmt.Errorf("error opening the file %s: %v", *s.ConfigFilePath, err)
	}
	defer file.Close()

	dec := yaml.NewDecoder(file)
	err = dec.Decode(&s.config)
	if err != nil {
		return err
	}

	s.ShelfService = &shelf.Service{}
	s.ShelfService.Start(s.config.Kitchen.ShelvesList)
	return nil
}

// used for internal testing
func (s *Service) getConfig() *Config {
	return s.config
}

func (s *Service) receiveOrders() {
	orders, err := s.OrderReader.ReadNextOrders(s.config.Kitchen.OrdersConfig.Rate)
	if err != nil {
		log.Errorf("Error reading next orders: %v", err)
	}
	s.processOrders(orders)
}

func (s *Service) processOrders(orders []*order.Order) {
	for i, order := range orders {
		s.placeInShelf(order)
		s.OrdersChan <- orders[i]
	}
}

func (s *Service) placeInShelf(order *order.Order) {
	s.ShelfService.AddToShelf(order)
}
