package kitchen

import (
	"time"

	"github.com/zmalik/ck-orders/pkg/shelf"
)

type Config struct {
	Kitchen Kitchen `yaml:"kitchen"`
}

type Kitchen struct {
	OrdersConfig OrdersConfig  `yaml:"orders"`
	ShelvesList  []shelf.Shelf `yaml:"shelves"`
}

type OrdersConfig struct {
	Rate     int           `yaml:"rate"`
	TimeUnit time.Duration `yaml:"timeUnit"`
}
