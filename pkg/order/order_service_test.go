package order

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFileOrderReader_ReadNextOrders(t *testing.T) {
	testOrderPath := "testdata/orders.json"
	reader := FileOrderReader{
		OrdersPath: &testOrderPath,
		offset:     0,
	}

	err := reader.LoadOrders()
	assert.Nil(t, err, "LoadOrders should load the orders without error")

	assert.Equal(t, 5, reader.getCount(), "getCount should return 5")
	orders, err := reader.ReadNextOrders(1)
	assert.Nil(t, err, "ReadNextOrders should return 1 order and no error")
	assert.Equal(t, 1, len(orders), "ReadNextOrders should return 1 order")
	assert.Equal(t, "a8cfcb76-7f24-4420-a5ba-d46dd77bdffd", orders[0].ID)

	orders, err = reader.ReadNextOrders(2)
	assert.Nil(t, err, "ReadNextOrders should return 2 orders and no error")
	assert.Equal(t, 2, len(orders))
	assert.Equal(t, "58e9b5fe-3fde-4a27-8e98-682e58a4a65d", orders[0].ID)
	assert.Equal(t, "2ec069e3-576f-48eb-869f-74a540ef840c", orders[1].ID)
}

func TestFileOrderReader_DataValidation(t *testing.T) {

	testOrderPath := "testdata/orders.json"
	reader := FileOrderReader{
		OrdersPath: &testOrderPath,
		offset:     0,
	}
	err := reader.LoadOrders()
	assert.Nil(t, err, "LoadOrders should load the orders without error")

	orders, err := reader.ReadNextOrders(1)
	assert.Nil(t, err, "ReadNextOrders should return 1 order and no error")
	assert.Equal(t, "a8cfcb76-7f24-4420-a5ba-d46dd77bdffd", orders[0].ID)
	assert.Equal(t, "Banana Split", orders[0].Name)
	assert.Equal(t, "frozen", orders[0].Temperature)
	assert.Equal(t, 20, orders[0].ShelfLifeSeconds)
	assert.Equal(t, 0.63, orders[0].DecayRate)
}

func TestFileOrderReader_ReadNotLoadedOrders(t *testing.T) {
	testOrderPath := "testdata/orders.json"
	reader := FileOrderReader{
		OrdersPath: &testOrderPath,
		offset:     0,
	}

	assert.Equal(t, 0, reader.getCount(), "getCount should return 0")

	_, err := reader.ReadNextOrders(1)
	assert.NotNil(t, err, "ReadNextOrders should return error as file wasn't loaded")
}

func TestFileOrderReader_MalformedData(t *testing.T) {
	testOrderPath := "testdata/malformed.json"
	reader := FileOrderReader{
		OrdersPath: &testOrderPath,
		offset:     0,
	}

	err := reader.LoadOrders()
	assert.NotNil(t, err, "malformed json file should return error when loading the file")
}

func TestFileOrderReader_NonExistentFile(t *testing.T) {
	testOrderPath := "testdata/non-existent-file.json"
	reader := FileOrderReader{
		OrdersPath: &testOrderPath,
		offset:     0,
	}

	err := reader.LoadOrders()
	assert.NotNil(t, err, "non-existent file should return error when loading the file")
}

func TestFileOrderReader_ZeroOrders(t *testing.T) {
	testOrderPath := "testdata/empty.json"
	reader := FileOrderReader{
		OrdersPath: &testOrderPath,
		offset:     0,
	}

	err := reader.LoadOrders()
	assert.Nil(t, err, "empty orders file shouldn't any return error when loading the file")
	orders, err := reader.ReadNextOrders(1)
	assert.Nil(t, err, "ReadNextOrders should return 0 orders and no error")
	assert.Equal(t, 0, len(orders), "ReadNextOrders should return 0 order")
}
