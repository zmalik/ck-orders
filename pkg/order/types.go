package order

// Order represents a delivery order
type Order struct {
	ID               string  `json:"id"`        // identifier of the order
	Name             string  `json:"name"`      // name of the dish
	Temperature      string  `json:"temp"`      // Preferred shelf storage temperature
	ShelfLifeSeconds int     `json:"shelfLife"` // Shelf wait max duration (seconds)
	DecayRate        float64 `json:"decayRate"` // Value deterioration modifier

	orderLifeSeconds   int
	shelfDecayModifier int
}

// IncrementAge increases the age of the order by one second
func (o *Order) IncrementAge() {
	o.orderLifeSeconds++
}

// SetShelfDecayModifier sets the shelfDecayModifier for the order
func (o *Order) SetShelfDecayModifier(shelfDecayModifier int) {
	o.shelfDecayModifier = shelfDecayModifier
}

// GetLifeSeconds returns the current life of ther order in seconds
func (o *Order) GetLifeSeconds() int {
	return o.orderLifeSeconds
}

// GetOrderValue returns the current inherent value of the order
func (o *Order) GetOrderValue() float64 {
	return (float64(o.ShelfLifeSeconds) - float64(o.orderLifeSeconds) - (o.DecayRate * float64(o.orderLifeSeconds) * float64(o.shelfDecayModifier))) / float64(o.ShelfLifeSeconds)
}
