package order

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sync"

	log "github.com/sirupsen/logrus"
)

type FileOrderReader struct {
	OrdersPath *string  // path to the orders file
	orders     []*Order // loaded orders for the simulation
	offset     int      // current offset for the simulation
	mutex      sync.Mutex
	Pause      bool // pause the orders, use for graceful shutdown of the service
}

// ReadNextOrders returns the next orders
// if there are not enough next orders, it returns the remaining orders
// if there are no orders, it returns an empty list
func (f *FileOrderReader) ReadNextOrders(count int) ([]*Order, error) {
	f.lock()
	defer f.unlock()
	if f.orders == nil {
		return nil, errors.New("no orders loaded in the simulation")
	}

	orders := make([]*Order, count)
	if f.offset >= len(f.orders) || f.Pause {
		return make([]*Order, 0), nil
	}
	i := 0
	for f.offset < len(f.orders) && i < count {
		orders[i] = f.orders[f.offset]
		i++
		f.offset++
	}
	if len(orders) > 0 {
		log.Infof("Received %d orders", len(orders))
	}
	return orders, nil
}

// LoadOrders loads the orders present in f.OrdersPath file
func (f *FileOrderReader) LoadOrders() error {
	file, err := os.Open(*f.OrdersPath)
	if err != nil {
		return fmt.Errorf("error opening the file %s: %v", *f.OrdersPath, err)
	}
	defer file.Close()

	dec := json.NewDecoder(file)
	err = dec.Decode(&f.orders)
	if err != nil {
		return err
	}
	return nil
}

func (f *FileOrderReader) getCount() int {
	if f.orders == nil {
		return 0
	}
	f.lock()
	defer f.unlock()
	return len(f.orders)
}

func (f *FileOrderReader) lock() {
	f.mutex.Lock()
}

func (f *FileOrderReader) unlock() {
	f.mutex.Unlock()
}
