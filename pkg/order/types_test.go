package order

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOrder(t *testing.T) {
	order := Order{
		ShelfLifeSeconds: 10,
		DecayRate:        0.5,
	}

	assert.Equal(t, 0, order.GetLifeSeconds(), "GetLifeSeconds should return 0")
	order.IncrementAge()
	//assert.Equal(t, 1, order.GetLifeSeconds(), "GetLifeSeconds should return 1")
	order.SetShelfDecayModifier(1)
	assert.Equal(t, 0.85, order.GetOrderValue(), "GetOrderValue should return positive value 0.85")
	order.SetShelfDecayModifier(18)
	assert.Equal(t, float64(0), order.GetOrderValue(), "GetOrderValue should return 0")
	order.SetShelfDecayModifier(20)
	assert.Equal(t, -0.1, order.GetOrderValue(), "GetOrderValue should return negative value 0")
}
