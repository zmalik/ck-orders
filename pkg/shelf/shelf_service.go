package shelf

import (
	"sync"

	log "github.com/sirupsen/logrus"
	"github.com/zmalik/ck-orders/pkg/order"
)

func (s *Service) Start(shelvesList []Shelf) {
	shelves := make(map[string]*Shelf, len(shelvesList))
	for i, shelf := range shelvesList {
		shelves[shelf.Temperature] = &shelvesList[i]
		shelves[shelf.Temperature].initOrders(shelf.Capacity)
	}
	s.Shelves = shelves
}

// ThrowExpiredOrders check for all orders in all the shelves and removes the expired orders
func (s *Service) ThrowExpiredOrders() {
	for _, shelf := range s.Shelves {
		shelf.throwExpiredOrders()
	}
}

// AddToShelf adds an order to the designated or overflow shelf
func (s *Service) AddToShelf(order *order.Order) {
	s.lock()
	defer s.unLock()
	if shelf, ok := s.Shelves[order.Temperature]; ok {
		if shelf.HasCapacity() {
			shelf.addOrder(order)
		} else {
			log.Infof("%s is full", shelf.Name)
			s.addToOverflowShelf(order)
		}
	} else {
		log.Errorf("cannot found a shelf for temperature: %s", order.Temperature)
	}
}

func (s *Shelf) initOrders(capacity int) {
	s.orders = make(map[string]*order.Order, capacity)
}

func (s *Service) moveOrderToNormalShelfOrRemoveOldest(shelfName string) {
	if shelf, ok := s.Shelves[shelfName]; ok {
		var oldestOrder *order.Order
		for _, order := range shelf.orders {
			if oldestOrder == nil {
				oldestOrder = order
			}
			if order.GetLifeSeconds() > oldestOrder.GetLifeSeconds() {
				oldestOrder = order
			}
			if s.Shelves[order.Temperature].HasCapacity() {
				shelf.removeOrder(order)
				s.Shelves[order.Temperature].addOrder(order)
				return
			}
		}
		log.Infof("removing oldest order %s from %s that has an age of %d seconds", oldestOrder.ID, shelf.Name, oldestOrder.GetLifeSeconds())
		shelf.removeOrder(oldestOrder)
	}
}

func (s *Service) addToOverflowShelf(order *order.Order) {
	if s.Shelves[OverFlowShelfTemperature].HasCapacity() {
		s.Shelves[OverFlowShelfTemperature].addOrder(order)
	} else {
		log.Warnf("OverflowShelf is full")
		s.moveOrderToNormalShelfOrRemoveOldest(OverFlowShelfTemperature)
		if s.Shelves[OverFlowShelfTemperature].HasCapacity() {
			s.Shelves[OverFlowShelfTemperature].addOrder(order)
		}
	}
}

// RetrieveOrder removes the order from the shelf
func (s *Shelf) RetrieveOrder(orderID string) bool {
	s.lock()
	defer s.unLock()
	if _, ok := s.orders[orderID]; ok {
		delete(s.orders, orderID)
		s.currentCapacity--
		return true
	}
	return false
}

// throwExpiredOrders check for all orders in the shelf and removes the expired orders
func (s *Shelf) throwExpiredOrders() {
	s.lock()
	defer s.unLock()
	for orderID, order := range s.orders {
		order.IncrementAge()
		if order.GetOrderValue() <= 0 {
			log.Infof("order %s expired after %d seconds with value %f", orderID, order.GetLifeSeconds(), order.GetOrderValue())
			delete(s.orders, orderID)
		}
	}
}

// HasCapacity checks if the shelf has capacity for orders
func (s *Shelf) HasCapacity() bool {
	return s.Capacity > s.currentCapacity
}

func (s *Shelf) removeOrder(order *order.Order) {
	s.lock()
	defer s.unLock()
	log.Infof("removed order %s from %s", order.ID, s.Name)
	delete(s.orders, order.ID)
	s.currentCapacity--
}

// addOrder adds the order to the shelf
func (s *Shelf) addOrder(order *order.Order) {
	s.lock()
	defer s.unLock()
	order.SetShelfDecayModifier(s.DecayModifier)
	s.orders[order.ID] = order
	s.currentCapacity++
	log.Infof("Order [%q-%q] added to the %s with value %f", order.ID, order.Name, s.Name, order.GetOrderValue())
}

func (s *Shelf) lock() {
	if s.mutex == nil {
		s.mutex = new(sync.Mutex)
	}
	s.mutex.Lock()
}

func (s *Shelf) unLock() {
	s.mutex.Unlock()
}

func (s *Service) lock() {
	if s.mutex == nil {
		s.mutex = new(sync.Mutex)
	}
	s.mutex.Lock()
}

func (s *Service) unLock() {
	s.mutex.Unlock()
}
