package shelf

import (
	"sync"

	"github.com/zmalik/ck-orders/pkg/order"
)

const (
	OverFlowShelfTemperature = "any"
)

type Shelf struct {
	Name          string `yaml:"name"`
	Temperature   string `yaml:"temperature"`
	Capacity      int    `yaml:"capacity"`
	DecayModifier int    `yaml:"decayModifier"`

	orders          map[string]*order.Order `yaml:"orders"`
	currentCapacity int
	mutex           *sync.Mutex
}

type Service struct {
	Shelves map[string]*Shelf // Shelves organised by temperature
	mutex   *sync.Mutex
}
