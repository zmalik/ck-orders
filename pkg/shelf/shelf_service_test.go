package shelf

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/zmalik/ck-orders/pkg/order"
)

func TestService_AddRemoveToShelf(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen Shelf",
			Temperature:   "frozen",
			Capacity:      10,
			DecayModifier: 1,
		},
	}
	s := Service{}
	s.Start(shelves)

	order := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	s.AddToShelf(order)

	assert.Equal(t, "McFlury", s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"].Name)
	s.Shelves["frozen"].removeOrder(order)
	assert.Nil(t, s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"])
}

func TestService_PickUpFromShelf(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen Shelf",
			Temperature:   "frozen",
			Capacity:      10,
			DecayModifier: 1,
		},
	}
	s := Service{}
	s.Start(shelves)

	order := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	s.AddToShelf(order)

	assert.Equal(t, "McFlury", s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"].Name)
	s.Shelves["frozen"].RetrieveOrder("58e9b5fe-3fde-4a27-8e98-682e58a4a65d")
	assert.Nil(t, s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"])
}

func TestService_AddToOverflowShelfAndRetrieve(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen shelf",
			Temperature:   "frozen",
			Capacity:      0,
			DecayModifier: 1,
		},
		{
			Name:          "Overflow shelf",
			Temperature:   OverFlowShelfTemperature,
			Capacity:      15,
			DecayModifier: 1,
		},
	}
	s := Service{}
	s.Start(shelves)

	order := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	s.AddToShelf(order)

	assert.Equal(t, "McFlury", s.Shelves[OverFlowShelfTemperature].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"].Name)
	assert.False(t, s.Shelves["frozen"].RetrieveOrder("58e9b5fe-3fde-4a27-8e98-682e58a4a65d"))
	assert.True(t, s.Shelves[OverFlowShelfTemperature].RetrieveOrder("58e9b5fe-3fde-4a27-8e98-682e58a4a65d"))
}

func TestService_AddToFullOverflowShelfAndRetrieve(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen shelf",
			Temperature:   "frozen",
			Capacity:      0,
			DecayModifier: 1,
		},
		{
			Name:          "Overflow shelf",
			Temperature:   OverFlowShelfTemperature,
			Capacity:      1,
			DecayModifier: 1,
		},
	}
	s := Service{}
	s.Start(shelves)

	order1 := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	order2 := &order.Order{
		ID:               "690b85f7-8c7d-4337-bd02-04e04454c826",
		Name:             "Yogurt",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	s.AddToShelf(order1)
	s.AddToShelf(order2)

	assert.Equal(t, "Yogurt", s.Shelves[OverFlowShelfTemperature].orders["690b85f7-8c7d-4337-bd02-04e04454c826"].Name)
	assert.False(t, s.Shelves["frozen"].RetrieveOrder("690b85f7-8c7d-4337-bd02-04e04454c826"))
	assert.False(t, s.Shelves[OverFlowShelfTemperature].RetrieveOrder("58e9b5fe-3fde-4a27-8e98-682e58a4a65d"))
	assert.True(t, s.Shelves[OverFlowShelfTemperature].RetrieveOrder("690b85f7-8c7d-4337-bd02-04e04454c826"))
}

func TestService_MoveOrdersFromOverflowShelf(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen shelf",
			Temperature:   "frozen",
			Capacity:      1,
			DecayModifier: 1,
		},
		{
			Name:          "Hot shelf",
			Temperature:   "hot",
			Capacity:      1,
			DecayModifier: 1,
		},
		{
			Name:          "Overflow shelf",
			Temperature:   OverFlowShelfTemperature,
			Capacity:      2,
			DecayModifier: 1,
		},
	}
	s := Service{}
	s.Start(shelves)

	order1 := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	order2 := &order.Order{
		ID:               "690b85f7-8c7d-4337-bd02-04e04454c826",
		Name:             "Yogurt",
		Temperature:      "frozen",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	order3 := &order.Order{
		ID:               "e868e485-c759-411b-b439-ca7086326bf6",
		Name:             "Beef Hash",
		Temperature:      "hot",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	order4 := &order.Order{
		ID:               "2bfc7ca9-d8ea-4625-9a03-6aa4b5fb635e",
		Name:             "Pork Chop",
		Temperature:      "hot",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	order5 := &order.Order{
		ID:               "c700ab30-d7bd-4e45-aafb-4329876c716c",
		Name:             "Vegan Pizza",
		Temperature:      "hot",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}
	order6 := &order.Order{
		ID:               "cf0932a9-533c-4603-bb1e-512c6e697b92",
		Name:             "Orange Chicken",
		Temperature:      "hot",
		ShelfLifeSeconds: 375,
		DecayRate:        0.4,
	}

	s.AddToShelf(order1)
	s.AddToShelf(order2)
	s.AddToShelf(order3)

	assert.Equal(t, "McFlury", s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"].Name)
	assert.Equal(t, "Yogurt", s.Shelves[OverFlowShelfTemperature].orders["690b85f7-8c7d-4337-bd02-04e04454c826"].Name)
	assert.Equal(t, "Beef Hash", s.Shelves["hot"].orders["e868e485-c759-411b-b439-ca7086326bf6"].Name)

	s.Shelves["frozen"].removeOrder(order1)
	s.AddToShelf(order4)
	assert.NotNil(t, s.Shelves[OverFlowShelfTemperature].orders["690b85f7-8c7d-4337-bd02-04e04454c826"])
	assert.NotNil(t, s.Shelves[OverFlowShelfTemperature].orders["2bfc7ca9-d8ea-4625-9a03-6aa4b5fb635e"])
	s.AddToShelf(order5)
	order4.IncrementAge()
	s.AddToShelf(order6)
	assert.NotNil(t, s.Shelves["frozen"].orders["690b85f7-8c7d-4337-bd02-04e04454c826"])
	// order4 is removed as its age is the oldest
	assert.Nil(t, s.Shelves[OverFlowShelfTemperature].orders["2bfc7ca9-d8ea-4625-9a03-6aa4b5fb635e"])
	// order5 is still present in overflow shelf
	assert.NotNil(t, s.Shelves[OverFlowShelfTemperature].orders["c700ab30-d7bd-4e45-aafb-4329876c716c"])
	assert.NotNil(t, s.Shelves[OverFlowShelfTemperature].orders["cf0932a9-533c-4603-bb1e-512c6e697b92"])

}

func TestService_OrderExpiration(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen shelf",
			Temperature:   "frozen",
			Capacity:      1,
			DecayModifier: 20,
		},
	}
	s := Service{}
	s.Start(shelves)

	order := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 10,
		DecayRate:        0.5,
	}

	s.AddToShelf(order)

	assert.Equal(t, "McFlury", s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"].Name)
	s.ThrowExpiredOrders()
	assert.Nil(t, s.Shelves["frozen"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"])
}

func TestService_OrderWithoutShelf(t *testing.T) {
	shelves := []Shelf{
		{
			Name:          "Frozen shelf",
			Temperature:   "hot",
			Capacity:      1,
			DecayModifier: 20,
		},
	}
	s := Service{}
	s.Start(shelves)

	order := &order.Order{
		ID:               "58e9b5fe-3fde-4a27-8e98-682e58a4a65d",
		Name:             "McFlury",
		Temperature:      "frozen",
		ShelfLifeSeconds: 10,
		DecayRate:        0.5,
	}

	s.AddToShelf(order)
	assert.Nil(t, s.Shelves["hot"].orders["58e9b5fe-3fde-4a27-8e98-682e58a4a65d"])
}
