package delivery

import (
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/zmalik/ck-orders/pkg/kitchen"
	"github.com/zmalik/ck-orders/pkg/order"
)

func TestService_RunDelivery(t *testing.T) {
	testConfigPath := "testdata/testconfig.yaml"
	testOrderPath := "testdata/orders.json"
	ordersIDs := make(chan *order.Order, 10)
	orderReader := &order.FileOrderReader{
		OrdersPath: &testOrderPath,
	}
	if err := orderReader.LoadOrders(); err != nil {
		log.Fatalf("Error loading the orders: %v", err)
	}

	ks := &kitchen.Service{
		ConfigFilePath: &testConfigPath,
		OrderReader:    orderReader,
		OrdersChan:     ordersIDs,
	}
	err := ks.Start()
	assert.Nil(t, err, "error for kitchen service should be nil")

	ds := &Service{
		ConfigFilePath:   &testConfigPath,
		KitchenService:   ks,
		OrdersChan:       ordersIDs,
		MarkPickedOrders: true,
	}

	err = ds.Start()
	assert.Nil(t, err, "error for delivery service should be nil")
	assert.Equal(t, 1, ds.config.Delivery.CourierConfig.RateMin)
	assert.Equal(t, 1, ds.config.Delivery.CourierConfig.RateMax)

	time.Sleep(2 * time.Second)

	assert.True(t, ds.isOrderPicked("a8cfcb76-7f24-4420-a5ba-d46dd77bdffd"))
	assert.True(t, ds.isOrderPicked("58e9b5fe-3fde-4a27-8e98-682e58a4a65d"))
}
