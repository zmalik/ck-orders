package delivery

import (
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/zmalik/ck-orders/pkg/kitchen"
	"github.com/zmalik/ck-orders/pkg/order"
	"gopkg.in/yaml.v3"
)

// Delivery Service watches the orders channel for pick up
// And sends courier with random delay inside configured range
type Service struct {
	ConfigFilePath *string // path to the Delivery Config file
	KitchenService *kitchen.Service
	config         *Config
	OrdersChan     chan *order.Order
	pickedOrders   map[string]bool
	mutex          sync.Mutex

	MarkPickedOrders bool
}

// Starts the Delivery Service
func (s *Service) Start() error {
	log.Infof("Started the delivery service...")
	if s.config == nil {
		log.Infof("Initializing the delivery configuration from: %s", *s.ConfigFilePath)
		err := s.initialize()
		if err != nil {
			log.Errorf("Error loading delivery configuration from %s", *s.ConfigFilePath)
			return err
		}
		s.pickedOrders = make(map[string]bool)
		log.Infof("Delivery Configuration loaded.")
	}
	go func() {
		for {
			order := <-s.OrdersChan
			log.Infof("Received pickup order of %s", order.ID)
			go s.askCourier(order)
		}
	}()
	return nil
}

// askCourier asks for a courier for a given order
// The courier can arrive in a random delay within configured time range
func (s *Service) askCourier(order *order.Order) {
	sleepDuration := time.Duration(random(
		s.config.Delivery.CourierConfig.RateMin, s.config.Delivery.CourierConfig.RateMax)) * s.config.Delivery.CourierConfig.TimeUnit
	time.Sleep(sleepDuration)
	if err := s.KitchenService.PickUpOrder(order); err != nil {
		log.Warnf("Error picking up order %s: %v", order.ID, err)
	} else {
		s.markAsPicked(order.ID)
		log.Infof("Order %s picked up after a wait time of %s with value %f", order.ID, sleepDuration, order.GetOrderValue())
	}
}

func (s *Service) initialize() error {
	file, err := os.Open(*s.ConfigFilePath)
	if err != nil {
		return fmt.Errorf("error opening the file %s: %v", *s.ConfigFilePath, err)
	}
	defer file.Close()

	dec := yaml.NewDecoder(file)
	return dec.Decode(&s.config)
}

// used for internal testing
// to verify that an order is picked up by a courier after a random delay
func (s *Service) markAsPicked(orderId string) {
	s.lock()
	defer s.unlock()
	if s.MarkPickedOrders {
		s.pickedOrders[orderId] = true
	}
}

// used for internal testing
// to verify that an order is picked up by a courier after a random delay
func (s *Service) isOrderPicked(orderId string) bool {
	s.lock()
	defer s.unlock()
	_, ok := s.pickedOrders[orderId]
	return ok
}

func random(min, max int) int {
	if min == max {
		return min
	}
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}

func (s *Service) lock() {
	s.mutex.Lock()
}

func (s *Service) unlock() {
	s.mutex.Unlock()
}
