package delivery

import "time"

type Config struct {
	Delivery Delivery `yaml:"delivery"`
}

type Delivery struct {
	CourierConfig CourierConfig `yaml:"courier"`
}

type CourierConfig struct {
	RateMin  int           `yaml:"rateMin"`
	RateMax  int           `yaml:"rateMax"`
	TimeUnit time.Duration `yaml:"timeUnit"`
}
