# Kitchens Orders Simulation

A Kitchen simulation challenge


### Pre-requirements

- docker installed in the host machine - [install docker](https://docs.docker.com/get-docker/)
- Go version > 1.14.0 installed in the host machine - [install go](https://golang.org/dl/)

### Run the simulation
```
make run
```

The project comes with 3 different simulation examples please read 
[running the example simulations](./docs/simulations.md) documentation to see how 
to run those different examples.

Please read the [configuration](./docs/configuration.md) documentation to see how to configure different options for the simulation.

## Architecture

The Kitchen Simulation consists of 4 services
- Kitchen
- Orders
- Shelf
- Delivery

These 4 services manage the incoming orders where Delivery and Kitchen service doesn't block each other. 
![Archictecture](./docs/images/architecture.png)

Please read [Development Guidelines](./docs/development.md) before adding any feature to the project.

## Demo
Short demo on how to run the simulation and doing a graceful shutdown using `ctrl+c`

![Demo](./docs/images/demo.gif)

## Services API documentation

Services API docs are available in [API Documentation](./docs/api)  

## Tests

The projects comes with unit and integration tests. Please read [Development Guidelines](./docs/development.md) for running them separately.

```
make test
go test ./pkg/... -cover
ok      github.com/zmalik/ck-orders/pkg/delivery        2.017s  coverage: 85.4% of statements
ok      github.com/zmalik/ck-orders/pkg/kitchen 1.017s  coverage: 88.4% of statements
ok      github.com/zmalik/ck-orders/pkg/order   0.017s  coverage: 100.0% of statements
ok      github.com/zmalik/ck-orders/pkg/shelf   0.015s  coverage: 100.0% of statements
go test ./integration/e2e_test.go -cover
ok      command-line-arguments  3.018s  coverage: [no statements]
```
