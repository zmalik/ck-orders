package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/zmalik/ck-orders/cmd/ck"
)

func main() {
	go ck.Run()

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)
	for {
		switch <-signals {
		case os.Interrupt, syscall.SIGTERM:
			logrus.Infoln("Gracefully shutting down the kitchen...")
			ck.PauseOrders()
			ck.Stop()
			os.Exit(1)
		}
	}

}
