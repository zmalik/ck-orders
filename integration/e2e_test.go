package integration

import (
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/zmalik/ck-orders/cmd/ck"
)

type AssertLogs struct {
	GoldenFile string
	T          *testing.T
}

func (l *AssertLogs) Write(p []byte) (n int, err error) {
	// The test asserts:
	// - order received
	// - delivery service received pick up order
	// - orders are added to the correct shelf
	// The test doesn't assert:
	// - delivery courier picks up the order with correct order value
	//   this is because of random pick up time which changes the inherent value always
	if !strings.Contains(string(p), "picked up after"){
		assert.Contains(l.T, l.GoldenFile, string(p))
	}
	fmt.Println(string(p))

	return 0, nil
}

func Test_E2E(t *testing.T) {
	err := flag.Set("order-file", "testdata/orders.json")
	assert.Nil(t, err)
	err = flag.Set("config", "testdata/config.yml")
	assert.Nil(t, err)
	logrus.SetReportCaller(false)
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableTimestamp:       true,
		DisableQuote:           true,
		DisableLevelTruncation: true,
	})
	content, err := ioutil.ReadFile("testdata/golden.log")
	if err != nil {
		t.Fail()
	}
	lw := &AssertLogs{
		GoldenFile: string(content),
		T:          t,
	}
	logrus.SetOutput(lw)
	go ck.Run()
	time.Sleep(3 * time.Second)
	ck.Stop()
}
