package ck

import (
	"flag"
	"fmt"
	"os"
	"path"
	"runtime"

	log "github.com/sirupsen/logrus"
	"github.com/zmalik/ck-orders/pkg/delivery"
	"github.com/zmalik/ck-orders/pkg/kitchen"
	"github.com/zmalik/ck-orders/pkg/order"
)

var (
	ordersPath           *string
	simulationConfigPath *string
	orderChannelBuffer   *int
	printHelp            *bool
	cs                   *kitchen.Service
)

func Run() {
	validateFlags()
	orderReader := &order.FileOrderReader{
		OrdersPath: ordersPath,
	}
	if err := orderReader.LoadOrders(); err != nil {
		log.Fatalf("Error loading the orders: %v", err)
	}

	ordersIDs := make(chan *order.Order, *orderChannelBuffer)

	cs = &kitchen.Service{
		ConfigFilePath: simulationConfigPath,
		OrderReader:    orderReader,
		OrdersChan:     ordersIDs,
	}

	if err := cs.Start(); err != nil {
		log.Fatalf("Error starting the kitchen: %v", err)
	}

	ds := &delivery.Service{
		ConfigFilePath: simulationConfigPath,
		KitchenService: cs,
		OrdersChan:     ordersIDs,
	}
	if err := ds.Start(); err != nil {
		log.Fatalf("Error starting the delivery: %v", err)
	}
}

func Stop() {
	cs.Stop = true
}

func PauseOrders() {
	cs.OrderReader.Pause = true
}

func init() {
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			filename := path.Base(f.File)
			return "", filename
		},
	})

	ordersPath = flag.String("order-file", "", "orders file path")
	simulationConfigPath = flag.String("config", "docs/examples/simple/config.yml", "configuration file path")
	orderChannelBuffer = flag.Int("orders-channel-buffer", 10, "channel buffer for the orders")
	printHelp = flag.Bool("help", false, "print usage")

	if *printHelp {
		printUsage()
		os.Exit(0)
	}
}

func validateFlags() {
	flag.Parse()
	if ordersPath == nil || *ordersPath == "" {
		printUsage()
		log.Fatalln("Missing orders file path")
	}
	if _, err := os.Stat(*ordersPath); err != nil {
		log.Fatalf("Path error for the file %s: %v", *ordersPath, err)
	}
	if _, err := os.Stat(*simulationConfigPath); err != nil {
		log.Fatalf("Path error for the file %s: %v", *simulationConfigPath, err)
	}
}

func printUsage() {
	doc := `Usage:
  ck-simulation [options]
Options:
  --order-file path to file containing the orders
      example:  --order-file=examples/simple/orders.json 
  --config path to configuration file of the kitchen simulation
      example:  --config=examples/simple/config.yaml
  --orders-channel-buffer channel buffer for the orders
      example:  --orders-channel-buffer=10
  --help print usage 
      example: --help
`
	fmt.Println(doc)

}
